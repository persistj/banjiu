import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    redirect: { name: 'index' }
  }, {
    path: '/index',
    name: 'index',
    component: resolve => require(['@/pages/index'], resolve),
    meta: { title: '首页' }
  }, {
    path: '/order',
    name: 'order',
    component: resolve => require(['@/pages/order/index'], resolve),
    meta: { title: '预约回收' }
  }, {
    path: '/history',
    name: 'history',
    component: resolve => require(['@/pages/history/index'], resolve),
    meta: { title: '斑鸠公益' }
  }, {
    path: '/news',
    name: 'news',
    component: resolve => require(['@/pages/news/index'], resolve),
    meta: { title: '斑鸠资讯' }
  }, {
    path: '/news-detail/:id',
    name: 'news-detail',
    component: resolve => require(['@/pages/news/detail'], resolve),
    meta: { title: '斑鸠资讯' }
  }, {
    path: '/about',
    name: 'about',
    component: resolve => require(['@/pages/about/index'], resolve),
    meta: { title: '关于我们' }
  }, {
    path: '/question',
    name: 'question',
    component: resolve => require(['@/pages/question/index'], resolve),
    meta: { title: '常见问题' }
  }],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
