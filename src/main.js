import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from './App'
import { Message, Breadcrumb, BreadcrumbItem } from 'iview';
import moment from 'moment'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'babel-polyfill' // IE 兼容

/**** 自定义 ****/
import router from './router'
import axios from './config/axios'
import createStore from './store'

/**** 工具 ****/
import filters from './util/dataConvers'

import 'swiper/dist/css/swiper.css'
import 'iview/dist/styles/iview.css';
import './css/index.css'

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(axios);
Vue.use(VueAwesomeSwiper);


Vue.config.productionTip     = false
Vue.prototype.moment         = moment
Vue.prototype.$Message       = Message

const store = createStore()

// 引入全局自定义数据处理函数
Object.keys(filters).forEach(key => {
  Vue.prototype[key] = filters[key]
})

const app = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})


