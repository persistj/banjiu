import _ from 'lodash'
import Vuex from 'vuex'
import news from './news/news'
import history from './news/history'

const isDev = process.env.NODE_ENV === 'development'

export default () => {
  const store = new Vuex.Store({
    strict: isDev,
    state: {
      token: ''
    },
    modules: {
      news,
      history,
    }
  })
  
  if (module.hot) {
    module.hot.accept([
      './news/news',
    ], () => {
      const newNews           = require('./news/news').default
      const newHistory          = require('./news/history').default
      
      store.hotUpdate({
        user: newNews,
        history: newHistory
      })
    })
  }
  
  return store
}
