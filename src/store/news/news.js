import scrollVuexGenerator from '../../util/vuexGennerator/scroll'
import { url, api } from '../../config/api'

const type = 'news'

export default scrollVuexGenerator({
  type,
  query: {
    type: 2,
    router: 'CMS_BJZX'
  },
  listApi: url + api.getNews,
  detailApi: url + api.getNewsDetail
})
