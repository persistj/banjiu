import scrollVuexGenerator from '../../util/vuexGennerator/scroll'
import { url, api } from '../../config/api'

const type = 'history'

export default scrollVuexGenerator({
  type,
  query: {
    type: 1,
    router: 'CMS_BJZX'
  },
  listApi: url + api.getNews
})
