import axios from 'axios'
import qs from 'qs'
import getHttpStatusText from '../util/getHttpStatusText'

export default () => {
  axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
  
  axios.interceptors.request.use(cfg => {
    if (cfg.method === 'post') {
      cfg.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
      cfg.data                    = qs.stringify(cfg.data)
      return cfg
    }
   
    return cfg
  }, err => [err.toString()])
  
  axios.interceptors.response.use(res => {
    const {
            data,
            status,
            data: {
              code = 0,
              message  = ''
            }
          } = res
    
    if (status !== 200) {
      return [`连接失败 code:${ status }`]
    }
  
    if (code !== 0) {
      return [message || '请求错误']
    }
    
    return [null, data]
  }, err => {
    const { response } = err
    let displayMessage = ''
    
    if (!response) {
      displayMessage = '未连接到服务器'
    }
    
    if (response && response.status) {      // 还是需要返回错误原信息,因为接口需要传递发送信息与接口的信息
      displayMessage = getHttpStatusText(response.status)
    }
    
    return [displayMessage]
  })
  
  return axios
}



