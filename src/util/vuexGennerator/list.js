import _ from 'lodash'
import axios from 'axios'

import { getName } from './util'

export default ({
                  type = '',
                  state = {},
                  query = {},
                  mutations = {},
                  actions = {},
  
                  listApi = '',
                  addApi = '',
                  removeApi = '',
                  detailApi = ''
                }) => {
  
  const listInitState = {
    loading: false,
    init: false,
    data: [],
    msg: '',
    total: 0,
    totalPage: 1,
    query: {
      // 当前页码
      page: 1,
      // 每页显示几条数据
      pagesize: 20,
      
      ...query
    }
  }
  
  const addInitState = {
    data: {},
    loading: false,
    msg: ''
  }
  
  const removeInitState = {
    data: {},
    loading: false,
    msg: ''
  }
  
  const detailInitState = {
    data: {},
    query: {},
    loading: false,
    msg: ''
  }
  
  const initState = {
    list: listInitState,
    detail: detailInitState,
    add: addInitState,
    remove: removeInitState,
    
    ...state
  }
  
  const _initState = _.clone(initState)
  
  // 列表
  const list_success = `list_${ type }_success`
  const list_reset   = `list_${ type }_reset`
  const list_loading = `list_${ type }_loading`
  const list_loaded  = `list_${ type }_loaded`
  
  // 查
  const detail_success = `detail_${ type }_success`
  const detail_reset   = `detail_${ type }_reset`
  const detail_loading = `detail_${ type }_loading`
  const detail_loaded  = `detail_${ type }_loaded`
  // 增
  const add_success    = `add_${ type }_success`
  const add_loading    = `add_${ type }_loading`
  const add_loaded     = `add_${ type }_loaded`
  
  // 删
  const remove_success = `remove_${ type }_success`
  const remove_loading = `remove_${ type }_loading`
  const remove_loaded  = `remove_${ type }_loaded`
  
  
  if (!_.isPlainObject(mutations)) {
    return
  }
  
  mutations = _.extend(mutations, {
    /** List **/
    [list_success] (state, payload) {
      state.list = {
        ...state.list,
        ...payload
      }
      
      state.list.msg = ''
    },
    [list_reset] (state, payload) {
      state.detail = _initState.list
    },
    [list_loading] (state, payload) {
      state.list.loading = true
    },
    [list_loaded] (state, payload) {
      state.list.loading = false
    },
    /** Add **/
    [add_success] (state, payload) {
      state.add.data = {
        ...state.add.data,
        ...payload
      }
      state.add.msg  = ''
    },
    [add_loading] (state, payload) {
      state.add.loading = true
    },
    [add_loaded] (state, payload) {
      state.add.loading = false
    },
    /** Remove **/
    [remove_success] (state, payload) {
      state.remove.data = {
        ...state.remove.data,
        ...payload
      }
      
      state.remove.msg = ''
    },
    [remove_loading] (state, payload) {
      state.remove.loading = true
    },
    [remove_loaded] (state, payload) {
      state.remove.loading = false
    },
    /** Detail **/
    [detail_success] (state, payload) {
      state.detail     = {
        ...state.detail,
        ...payload
      }
      state.detail.msg = ''
    },
    [detail_reset] (state, payload) {
      state.detail = _initState.detail
    },
    [detail_loading] (state, payload) {
      state.detail.loading = true
    },
    [detail_loaded] (state, payload) {
      state.detail.loading = false
    }
  })
  
  return {
    state: initState,
    mutations: mutations,
    actions: _.extend(actions, {
      // 获取列表 / 增删改查的方法
      [getName(type, 'get', 'list')]: async ({ state, commit, rootState }, params = {}) => {
        const {
                list: {
                  init,
                  query,
                  data
                }
              } = state
        
        const assignParams            = Object.assign({}, query, params)
        const { ignoreCache = false } = params
        
        if (!ignoreCache && init && _.isEqual(query, assignParams)) {
          return [null, data]
        }
        
        commit(detail_reset)
        commit(list_loading)
        
        const [err, res] = await axios.post(listApi, assignParams)
        
        commit(list_loaded)
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const {
                data: listData,
                total,
                totalpage
              } = res
        
        commit(list_success, {
          init: true,
          data: listData,
          query: assignParams,
          totalPage: totalpage,
          total
        })
        
        return [null, res]
      },
      [getName(type, 'add')]: async ({ state, commit, rootState }, params = {}) => {
        commit(add_loading)
        
        const [err, res] = await axios.post(addApi, params)
        
        commit(add_loaded)
        
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const { data } = res
        
        commit(add_success, { ...data })
        
        return [null, data]
      },
      [getName(type, 'remove')]: async ({ state, commit, rootState }, params = {}) => {
        commit(remove_loading)
        
        const [err, res] = await axios.post(removeApi, params)
        
        commit(remove_loaded)
        
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const { data } = res
        
        commit(remove_success, { ...data })
        
        return [null, data]
      },
      [getName(type, 'get')]: async ({ state, commit, rootState }, params = {}) => {
        const {
                detail: {
                  query,
                  data: oldData
                }
              } = state
        
        const { ignoreCache = false } = params
        
        if (!ignoreCache && _.isEqual(query, params)) {
          return [null, oldData]
        }
        
        commit(detail_reset)
        commit(detail_loading)
        
        const [err, res] = await axios.post(detailApi, params)
        
        commit(detail_loaded)
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const { data = {} } = res
        
        commit(detail_success, {
          data,
          query: params
        })
        
        return [null, data]
      }
    })
  }
}
