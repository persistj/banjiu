import _ from 'lodash'
import axios from 'axios'

import { getName } from './util'

export default ({
                  type = '',
                  state = {},
                  params = {},
                  getters = {},
                  mutations = {},
                  actions = {},
                  api = ''
                }) => {
  
  
  const initState = {
    loading: false,
    init: false,
    data: [],
    query: {},
    msg: '',
    
    ...state
  }
  
  const  _initState = _.clone(initState)
  
  if (!_.isPlainObject(mutations)) {
    return
  }
  
  const common_success = `${ type }_success`
  const common_reset   = `${ type }_reset`
  const common_loading = `${ type }_loading`
  const common_loaded  = `${ type }_loaded`
  
  mutations = _.extend(mutations, {
    [common_success] (state, payload) {
      state     = _.extend(
        state,
        payload
      )
      state.msg = ''
    },
    [common_reset] (state, payload) {
      state = _.extend(state, _initState)
    },
    [common_loading] (state, payload) {
      state.loading = true
    },
    [common_loaded] (state, payload) {
      state.loading = false
    }
  })
  
  return {
    state: initState,
    getters: getters,
    mutations: mutations,
    actions: _.extend(actions, {
      [getName(type, 'get')]: async ({ state, commit, rootState }, _params = {}) => {
        const {
                loading,
                query,
                data,
                init
              } = state
        
        const assignParams            = Object.assign({}, params, _params)
        const { ignoreCache = false } = _params
        
        if (!ignoreCache && init && _.isEqual(query, assignParams)) {
          return [null, data]
        }
        
        commit(common_reset)
        commit(common_loading)
        
        const [err, res] = await axios.post(api, assignParams)
        
        commit(common_loaded)
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const { data: commonData } = res
        
        commit(common_success, {
          init: true,
          data: commonData,
          query: assignParams
        })
        
        return [null, commonData]
      }
    })
  }
}
