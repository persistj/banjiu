import _ from 'lodash'
import axios from 'axios'

import { getName } from './util'

export default ({
                  type = '',
                  state = {},
                  params = {},
                  mutations = {},
                  actions = {},
                  api = ''
                }) => {
  
  
  const initState = {
    loading: false,
    init: false,
    data: [],
    query: {},
    msg: '',
    
    ...state
  }
  
  if (!_.isPlainObject(mutations)) {
    return
  }
  
  const dict_success = `dict_${ type }_success`
  const dict_reset   = `dict_${ type }_reset`
  const dict_loading = `dict_${ type }_loading`
  const dict_loaded  = `dict_${ type }_loaded`
  
  mutations = _.extend(mutations, {
    [dict_success] (state, payload) {
      state     = _.extend(
        state,
        payload
      )
      state.msg = ''
    },
    [dict_reset] (state, payload) {
      state = initState
    },
    [dict_loading] (state, payload) {
      state.loading = true
    },
    [dict_loaded] (state, payload) {
      state.loading = false
    }
  })
  
  return {
    state: initState,
    mutations: mutations,
    actions: _.extend(actions, {
      [getName(type, 'get')]: async ({ state, commit, rootState }, _params = {}) => {
        const {
                loading,
                query,
                data,
                init
              } = state
        
        const assignParams            = Object.assign({}, params, _params)
        const { ignoreCache = false } = _params
        
        if (!ignoreCache && init && _.isEqual(query, assignParams)) {
          return [null, data]
        }
        
        commit(dict_loading)
        
        const [err, res] = await axios.post(api, assignParams)
        
        commit(dict_loaded)
        
        if (err) {
          if (res) {
            return [err, res]
          }
          return [err]
        }
        
        const { data: dictData } = res
        
        commit(dict_success, {
          init: true,
          data: dictData,
          query: assignParams
        })

        return [null, dictData]
      }
    })
  }
}
